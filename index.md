# Collection of online resources and MOOCS in bioinformatics

## Warning(s)

- Check for newer versions
- **Your** input is welcome (impressions about the MOOCS or suggestions for example)


## Resources

- [France Université Numérique](https://www.fun-mooc.fr/) is a great repository of online courses.
- [Openclassroom](https://openclassrooms.com)
- [Goblet: Global Organisation for Bioinformatics Learning, Education and Training](https://www.mygoblet.org/) : pas vraiment une source de MOOCS mais plutôt une [liste de cours et de documents](https://www.mygoblet.org/training-portal). Signalé par (l'excellent blog) [bioinfo-fr.net](https://bioinfo-fr.net/former-et-se-former-en-bioinformatique-avec-goblet) ; il y a aussi un [article dans Bioinformatics](https://academic.oup.com/bioinformatics/article/31/1/140/2366130)

## Moocs

- Plus de 50 tutos de bioinfo : depuis les bases de GNU/Linux, les outils de bases (git, nextflow, conda...) l'analyse de données [Chaîne youtube de Simon Cockell](https://www.youtube.com/c/SimonCockell/videos)
- Informatique générale, GNU/Linux et unix
    - [Maîtriser le shell Bash](https://www.fun-mooc.fr/courses/course-v1:univ-reunion+128001+session02/about)
    - [Introduction to computer science](https://online-learning.harvard.edu/course/cs50-introduction-computer-science) Il s’agit  d'un  cours réellement enseigné aux étudiants de Harvard. Toutes les conférences sont filmées à Harvard et mises en ligne, avec les devoirs, les vidéos de tutoriel et toutes les notes de support . Les devoirs sont notés.
Le cours commence à partir des bases, expliquant le binaire et utilisant le langage de programmation visuel basé sur des blocs Scratch avant de passer à C.
 La difficulté monte très rapidement à partir de là, avec des discussions approfondies sur la façon dont l'ordinateur traite le code, comment l'information est stockés en mémoire et quelles sont les structures de données. Il passe ensuite au HTML, Javascript et Python. À la fin, vous exécutez un serveur Web et extrayez des données des API.
- Analyse de données, statistique
    - [Introduction à la statistique avec R](https://www.fun-mooc.fr/courses/course-v1:UPSUD+42001+session09/about)
    - [Nettoyez et décrivez votre jeu de données](https://openclassrooms.com/fr/courses/4525266-decrivez-et-nettoyez-votre-jeu-de-donnees) Un cours pour apprendre à apréhender un jeu de données et réaliser quelques tests simples. Les instructions sont données en Python et en R pour que l'utilisateur puisse suivre le cours avec le langage de son choix.
    - [Découvrez les librairies Python pour le data science](https://openclassrooms.com/fr/courses/4452741-decouvrez-les-librairies-python-pour-la-data-science) Ce cours donne les bases de l'utilisation de notebooks Jupyter et de l'utilisation de Pandas, Numpy et Matplotlib
    - ~~[Exploratory multivariate data analysis](https://www.fun-mooc.fr/courses/course-v1:agrocampusouest+40001EN+session06/about) (FUN, en Anglais)~~ remplacé par [Analyse des données multidimensionnelles](https://www.fun-mooc.fr/fr/cours/analyse-des-donnes-multidimensionnelles/) sur FUN
    - voir aussi les ressources de [wikistat](http://wikistat.fr/) (cf. section Machine learning)
- Machine learning
    - [Algèbre linéaire à la Khan academy](https://fr.khanacademy.org/math/linear-algebra) pas un MOOC, mais très utile pour réviser les bases
    - [Introduction to machine learning](https://www.coursera.org/learn/machine-learning-duke) (coursera)
    - [Machine learning in Python with scikit-learn](https://www.fun-mooc.fr/en/courses/machine-learning-python-scikit-learn/) MOOC Inria sur FUN
    - [Machine learning](https://www.coursera.org/learn/machine-learning) : advanced Stanford class by Andrew Ng
    - (Site web avec cours, pas vraiment un MOOC) [WikiStat : Statistique et Machine Learning de Statisticien à Data Scientist](http://wikistat.fr/)
        - en cours de migration vers [github wikistat](http://github.com/wikistat/) avec scénarios et tutoriels sous forme de notebooks R et python
        - [Apprentissage statistique avec Python scikit-learn](https://www.math.univ-toulouse.fr/~besse/Wikistat/pdf/st-tutor3-python-scikit.pdf)
    - Chaîne youtube [MachineLearnia](https://www.youtube.com/channel/UCmpptkXu8iIFe6kfDK5o7VQ)
        - [Python spécial machine learning (34 vidéos)](https://www.youtube.com/playlist?list=PLO_fdPEVlfKqMDNmCFzQISI2H_nJcEDJq) commence par des rappels de Python, des librairies standards jusqu'à sklearn et les techniques d'apprentissage
        - [Machine learning formation complète (10 vidéos)](https://www.youtube.com/playlist?list=PLO_fdPEVlfKqUF5BPKjGSh7aV9aBshrpY)
- Python
    - [Python : des fondamentaux à l'utilisation du langage](https://www.fun-mooc.fr/courses/inria/41001S03/session03/about)
    - [Découvrez les librairies Python pour le data science](https://openclassrooms.com/fr/courses/4452741-decouvrez-les-librairies-python-pour-la-data-science) Ce cours donne les bases de l'utilisation de notebooks Jupyter et de l'utilisation de Pandas, Numpy et Matplotlib
    - [Python avancé](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python)
Comme son nom l'indique, un cours pour apprendre à programmer en Python. Il est intéressant parce qu'il suit une logique de POO, et il va assez loin dans l'utilisation du langage, avec les test unitaires, le threading, la gestion de réseaux,...
    - Chaîne youtube [MachineLearnia](https://www.youtube.com/channel/UCmpptkXu8iIFe6kfDK5o7VQ) : le début de [Python spécial machine learning (34 vidéos)](https://www.youtube.com/playlist?list=PLO_fdPEVlfKqMDNmCFzQISI2H_nJcEDJq) y compris jusqu'à la programmation orientée-objets
    - [Développez votre site web avec le framework django](https://openclassrooms.com/fr/courses/1871271-developpez-votre-site-web-avec-le-framework-django) Pour développer des sites web avec Python
- Java
    - [Initiation à la programmation (en Java)](https://www.coursera.org/learn/initiation-programmation-java)
- C++
    - [Initiation à la programmation (en C++)](https://fr.coursera.org/learn/initiation-programmation-cpp)
    - [Introduction à la programmation orientée objet (en C++)](https://fr.coursera.org/learn/programmation-orientee-objet-cpp)
- Bioinformatique (version info)
    - [Bioinformatique : algorithmes et génomes](https://www.fun-mooc.fr/courses/course-v1:inria+41003+session03/about)
    - [Bioinformatics specialization (by Pavel Pezner and Phillip Compeau)](https://www.coursera.org/specializations/bioinformatics): composed of seven courses, all excellent
- Bioinformatique (version bio)
    - [BIG : BioInformatique pour la Génétique Médicale](https://www.fun-mooc.fr/courses/course-v1:USPC+37028+session01/about) (FUN, en Français)
- Données massives : bases de données, Web Sémantique...
    - [Bases de données relationnelles](http://flot.sillages.info/?portfolio=bases-de-donnees-relationnelles)
    - [Initiez-vous à l'algèbre relationnelle avec le langage SQL](https://openclassrooms.com/fr/courses/4449026-initiez-vous-a-lalgebre-relationnelle-avec-le-langage-sql) Approche très pédagogue de l'algèbre relationnelle
    - [From database to big data](https://www.fun-mooc.fr/courses/course-v1:UCA+107004+session01/about) SQL, noSQL
    - [Data science : Fondamentaux pour le Big Data](https://www.fun-mooc.fr/courses/course-v1:MinesTelecom+04006+session10/about) (assez maths)
    - [Web sémantique et Web de données](https://www.fun-mooc.fr/courses/course-v1:inria+41002+session03/about)
- Science reproductible
    - [Recherche reproductible : principes méthodologiques pour une science transparente](https://www.fun-mooc.fr/courses/course-v1:inria+41016+session01bis/about) (FUN, en français)
- Communication
    - [Améliorez l'impact de vos présentations](https://openclassrooms.com/fr/courses/3013891-ameliorez-limpact-de-vos-presentations) De nombreux conseils pour réaliser des diaporamas impactants et agréables à l'oeil)
    - [Prenez la parole en public](https://openclassrooms.com/fr/courses/4577696-prenez-la-parole-en-public)

# Todo

- Python3 (surtout pour les étudiant-e-s qui n'ont pas validé PIP)

