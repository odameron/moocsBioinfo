# MOOCS : consignes et proposition de parcours-types

Pour ceux qui n'ont pas trouvé de projet/compétition qui pourraient vous intéresser, vous pouvez vous inscrire (après validation par nos soins) à un parcours MOOCs qu'Emmanuelle et Olivier vous ont concocté. Ces parcours adaptés vous permettront de progresser et de consolider vos connaissances pour l'entrée en M2. Ils donneront lieu à la rédaction d'un mémoire. Si vous choisissez cette solution voici quelques consignes :


## Consignes

- **Consigne1 :** on va vous demander de prendre des captures d'écran des résultats des différentes étapes d'évaluation du MOOC...
- **Consigne 2:** ...donc pensez à vous inscrire avec un pseudo qui correspond à votre identité
- **Consigne 3:** attention, certains MOOCS vous permettent de suivre les cours gratuitement, mais vous proposent de payer si vous voulez une certification ou accès à des ressources ou activités supplémentaires. Le master de bioinfo **ne vous demande pas** de payer ces suppléments
- **Consigne 4:** nous vous proposons certaines combinaisons de MOOCS. Nous **devons
valider votre choix** avant que vous ne vous lanciez (on sera en particulier attenti-f-ve-s à ce que vous ne choisissiez pas une combinaison trop facile ou trop difficile)
- **Consigne 5:** vous pouvez aussi aménager ces parcours en prenant d'autres MOOCS de la liste (ou d'ailleurs). Là aussi (surtout), nous devons valider votre choix
- **Consigne 6:** si vous avez du mal à faire votre choix, on peut convenir que vous essayez plusieurs MOOCS pendant une semaine avant de vous décider. Là encore, nous devons valider votre la sélection de MOOCS que vous allez essayer ET la combinaison que vous proposerez à l'issue de la semaine
- **Consigne 7:** si vous trouvez d'autres MOOCS potentiellement pertinents (ou si vous souhaitez ajouter un avis sur ceux de la liste pour aider les autres étudiant-e-s à choisir), vos suggestions sont les bienvenues
- **Consigne 8:** on créera un salon textuel par MOOC sur le serveur discord pour que vous puissiez discuter avec les autres étudiants qui ont choisi les mêmes MOOCS que vous

La liste des MOOCS potentiellement intéressants :
https://gitlab.com/odameron/moocsBioinfo
en complément, vous trouverez aussi des ressources à
https://gitlab.com/odameron/lectureBioinfo


## Parcours types

### Parcours 1 (+++)

- Fondamentaux Big Data (fun)
- Machine Learning (coursera)


### ~~Parcours 2 (+  OU ++ OU ++)~~

- ~~Bioinfo Génétique Médicale (fun)~~
- ~~1 ou 2 au choix parmi~~
    - ~~Python3 (fun)~~
    - ~~Machine Learning (coursera)~~
    - ~~Exploratory Multivariate Analysis (fun)~~ remplacé par [Analyse des données multidimensionnelles](https://www.fun-mooc.fr/fr/cours/analyse-des-donnes-multidimensionnelles/) sur FUN


### Parcours 3 (+) : renforcement, choisir 2 parmi les 3

- Intro à R (fun)
- Maitriser le shell Bash (fun)
- Python 3 (fun)


### Parcours 4 (++ OU +++)

- Web sémantique et web des données (fun)
- 1 au choix parmi
    - from Database to Big Data (fun)
    - Fondamentaux Big Data (fun)


### Parcours 5 (++)

- ~~Exploratory Multivariate Analysis (fun)~~ remplacé par [Analyse des données multidimensionnelles](https://www.fun-mooc.fr/fr/cours/analyse-des-donnes-multidimensionnelles/) sur FUN
- Machine Learning (coursera)


Bonne journée,

Emmanuelle, Olivier et Annabelle


